
// PIC16F1829 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = ON      // Power-up Timer Enable (PWRT disabled)
#pragma config MCLRE = OFF       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Memory Code Protection (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = OFF        // Internal/External Switchover (Internal/External Switchover mode is enabled)
#pragma config FCMEN = OFF       // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is enabled)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config PLLEN = OFF      // PLL Enable (4x PLL disabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LVP = ON         // Low-Voltage Programming Enable (Low-voltage programming enabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#define _XTAL_FREQ  16000000
#define THRESHOULD 8

int main() {

    OSCCON  =   0b01111010; //16MHz
    
    TRISA   =   0b00000000;
    TRISB   =   0b00010000; //use RB4/AN10 as input
    TRISC   =   0b00000000;
    
    ANSELA  =   0b00000000;
    ANSELB  =   0b00010000; //use RB4/AN10 as Analog
    ANSELC  =   0b00000000;
    
    PORTA   =   0b00000000;
    PORTC   =   0b00000000;
    PORTB   =   0b00000000;
    
    //For A.D. Converter
    ADCON0  =   0b00101001;
    ADCON1  =   0b10100011;

    FVRCON = 0b10000001;
    
    
    PR2 = (unsigned char)159;

    PIR1 = 0x00;
    CCP3CON = 0b00001100;
    T2CON =   0b00000001;
    CCPTMRS = 0;
    
    T2CONbits.T2CKPS = 0b10; 
    T2CONbits.TMR2ON = 0;
    
    CCPR3L = (unsigned char)640/4;
    CCPTMRS = 0b00000000;

    __delay_us(20);
    
    //int thread = 0, out = 1;
    int input;
    
    ADCON0bits.GO = 1;
    while(!FVRCONbits.FVRRDY);
    while(ADCON0bits.GO);
    int input0=ADRESL+ADRESH*256-THRESHOULD;
    
    while(1){
        ADCON0bits.GO = 1;
        while(ADCON0bits.GO);
        input=ADRESL+ADRESH*256;

        if(input<input0){
            RA4 = 0;
            RA5 = 1;
            T2CONbits.TMR2ON = 1;
        }else{
            RA4 = 1;
            RA5 = 0;
            T2CONbits.TMR2ON = 0;
        } 
    }

    return (EXIT_SUCCESS);
}